/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <View style={{flex:1, backgroundColor:'#F7F7F7'}}>
      <View style={{padding:20, flex:1, justifyContent:'center'}}>
        <View style={{width:'100%', backgroundColor:'#FFFFFF', padding:16, borderRadius:10, alignItems:'center'}}>
          <View style={{width:170, justifyContent:'center', height:36, backgroundColor:'#002558', marginTop:-34, borderRadius:100}}>
            <Text style={{textAlign:'center', fontSize:16, color:'white'}}>
              Digital Approval
            </Text>
          </View>
          <Image source={require('./image.png')} style={{marginTop: 45,width:127,height:55}} />
          <View style={{width:'100%',marginTop:38,borderRadius:5,borderWidth:1,borderColor:'#002558'}}>
            {/* <Image source={require('./email.png')} style={{width:16,height:16}}/> */}
            <TextInput style={{marginLeft:16}} placeholder='Username'/>
          </View>
          <View style={{width:'100%',marginTop:24,borderRadius:5,borderWidth:1,borderColor:'#002558'}}>
            {/* <Image source={require('./lock.png')} style={{width:16,height:16}}/> */}
            <TextInput style={{marginLeft:16}} placeholder='Password'/>
          </View>
          <Text style={{fontSize:14,marginTop:5,color:'#287AE5',fontStyle:'italic',alignSelf:'flex-end', marginTop:16}}>
            Reset Password
          </Text>
          <TouchableOpacity style={{width:'100%',borderRadius:5,justifyContent:'center', paddingHorizontal:120, marginTop:24,paddingVertical:14, backgroundColor:'#287AE5'}}>
            <Text style={{fontSize:16,color:'white',fontWeight:'bold', justifyContent:'center'}}>
              LOGIN
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'blue'
  },
});

export default App;
